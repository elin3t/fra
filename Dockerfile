FROM python:3.9-slim-buster

# Install dependencies
RUN apt-get update && apt-get install -y \
    build-essential \
    libpq-dev \
    postgresql-client \
    redis-tools \
    curl

# Install Poetry via pip
RUN pip install poetry

# Create and set the working directory
RUN mkdir /app
WORKDIR /app

# Copy the poetry.lock and pyproject.toml files
COPY poetry.lock pyproject.toml ./

# Install Python dependencies
RUN poetry config virtualenvs.create false && poetry install --no-root --no-interaction --no-ansi

# Copy the application code
COPY . .

# Copy entrypoint script
COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

# Expose port 5000 for the Flask app
EXPOSE 5000

# Set entrypoint to run database migrations and start Gunicorn web server
ENTRYPOINT ["/entrypoint.sh"]
