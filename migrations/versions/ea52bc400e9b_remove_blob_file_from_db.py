"""remove blob file from db

Revision ID: ea52bc400e9b
Revises: 222c027bbdec
Create Date: 2022-11-21 10:39:33.917633

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = "ea52bc400e9b"
down_revision = "222c027bbdec"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column("raw_files", "data")
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column(
        "raw_files",
        sa.Column("data", postgresql.BYTEA(), autoincrement=False, nullable=True),
    )
    # ### end Alembic commands ###
