#!/bin/bash

# Apply database migrations
flask --app server.app db upgrade

# Start Gunicorn web server
exec gunicorn 'server.app:create_app()' -b 0.0.0.0:5000 --workers 4
